import re
import sys
import dateutil.parser
import datetime

rfc3399 = r"\d{4}-(?:0[1-9]|1[0-2])-(?:0[1-9]|[12][0-9]|3[01]) (?:[01][0-9]|2[0-3]):(?:[0-5][0-9]):(?:[0-5][0-9]|60)(?:\.[0-9]+)?(?:Z|(?:\+|-)(?:[01][0-9]|2[0-3]):(?:[0-5][0-9]))"


def parse(fn, output_fn=None):
    print("parsing file %s" % fn)
    print("output in %s" % output_fn)
    r_start = re.compile(r"\[\]\[\]\[\]running command (.+?), time=(%s)" % rfc3399)
    r_stop = re.compile(r"\[\]\[\]\[\]finished command (.+?), status=(.+), time=(%s)" % rfc3399)
    r_host = re.compile(r"\[\]\[\]\[\]hostname: (.+)")


    starts = []
    stops = []
    hotname = None

    for line in open(fn):
        if "[][][]" not in line:
            continue
        m = r_host.match(line)
        if m:
            hostname = m.group(1)
        m = r_start.match(line)
        if m:
            command = m.group(1)
            start_time = m.group(2)
            start_time = dateutil.parser.parse(start_time)
            starts.append((command, start_time))
            continue
        m = r_stop.match(line)
        if m:
            command = m.group(1)
            status = m.group(2)
            stop_time = m.group(3)
            stop_time = dateutil.parser.parse(stop_time)
            stops.append((command, stop_time, status))
            continue

    performances = []
    for start, stop in zip(starts, stops):
        if start[0] != stop[0]:
            print("problem")
            print(start)
            print(stop)
            continue

        delta = stop[1] - start[1]
        performances.append((start[0], stop[2], delta.seconds + delta.microseconds * 1E-6))

    if output_fn is None:
        output_fn = "performance_%s_%s.txt" % (hostname, datetime.datetime.today())
    fo = open(output_fn, 'w')
    for p in performances:
        fo.write("%s %s %s\n" % (p[0], p[1], p[2]))

if __name__ == "__main__":
    parse(sys.argv[1])
