export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh
lsetup "root 6.18.04-x86_64-centos7-gcc8-opt"
root -l -q -e "RooRealVar x"
python -c "import ROOT; ROOT.RooRealVar()"
