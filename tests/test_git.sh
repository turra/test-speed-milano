export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh
lsetup git
cd /tmp && git clone ssh://git@gitlab.cern.ch:7999/atlas-hgam-sw/HGamCore.git
rm -rf /tmp/HGamCore
cd ~ && git clone ssh://git@gitlab.cern.ch:7999/atlas-hgam-sw/HGamCore.git htemp
rm -rf ~/htemp

