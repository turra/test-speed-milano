from convert_command import convert_command
from parse import parse
import os
import subprocess
import random
from datetime import datetime

machines = ['proof-01.mi.infn.it', 'proof-02.mi.infn.it', 'proof-03.mi.infn.it', 'proof-04.mi.infn.it', 'proof-05.mi.infn.it', 'proof-06.mi.infn.it', 'proof-07.mi.infn.it', 'proof-08.mi.infn.it', 'proof-09.mi.infn.it', 'proof-10.mi.infn.it', 'lxplus']
tests = ["test_git.sh", "test_asetup.sh", "test_cvmfs.sh", "test_condor.sh"]

import argparse

parser = argparse.ArgumentParser()
parser.add_argument('--machine')
parser.add_argument('--test')
args = parser.parse_args()

if args.machine is None:
    machine = random.choice(machines)
else:
    machine = args.machine
if args.test is None:
    test = random.choice(tests)
else:
    test = args.test

print("running test %s on machine %s" % (test, machine))

from pathlib import Path
Path("log").mkdir(parents=True, exist_ok=True)
Path("results").mkdir(parents=True, exist_ok=True)

command = convert_command(os.path.join('tests', test))
now = datetime.today()
log = 'log/log__%s__%s__%s' % (machine, os.path.basename(command), now)
print("running ssh %s < %s > '%s'" % (machine, command, log))
subprocess.run("ssh %s < %s > '%s'" % (machine, command, log), shell=True)
parse(log, 'results/performance__%s__%s__%s' % (machine, os.path.basename(command), now))
