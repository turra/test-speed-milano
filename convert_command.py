import sys
import os
import stat


def convert_command(fn):
    f = open(fn)

    lines = ['#!/bin/env sh', 'echo "[][][]hostname: `hostname`"']
    for line in f:
        if not line:
            continue
        lines.append('echo "[][][]running command %s, time=`date --rfc-3339 ns`"\n' % line.strip().replace(r'"', r'\"'))
        lines.append(line)
        lines.append('echo "[][][]finished command %s, status=$?, time=`date --rfc-3339 ns`"\n' % line.strip().replace(r'"', r'\"'))

    output = '\n'.join(lines)

    fnout = os.path.splitext(fn)[0] + "_timed.sh"

    fout = open(fnout, "w")
    fout.write(output)
    st = os.stat(fnout)
    os.chmod(fnout, st.st_mode | stat.S_IEXEC)
    return fnout


if __name__ == "__main__":
    fn = sys.argv[1]
    fout = convert_command(fn)
    print("new command written in %s" % fout)
